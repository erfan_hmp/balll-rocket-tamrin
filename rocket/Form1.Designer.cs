﻿namespace rocket
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.blueRocket = new System.Windows.Forms.Label();
            this.redRocket = new System.Windows.Forms.Label();
            this.redScore = new System.Windows.Forms.Label();
            this.blueScore = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonResetScore = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(434, 418);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(620, 418);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Stop";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // blueRocket
            // 
            this.blueRocket.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.blueRocket.BackColor = System.Drawing.Color.Blue;
            this.blueRocket.Location = new System.Drawing.Point(277, 55);
            this.blueRocket.Name = "blueRocket";
            this.blueRocket.Size = new System.Drawing.Size(200, 23);
            this.blueRocket.TabIndex = 2;
            // 
            // redRocket
            // 
            this.redRocket.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.redRocket.BackColor = System.Drawing.Color.Red;
            this.redRocket.Location = new System.Drawing.Point(277, 382);
            this.redRocket.Name = "redRocket";
            this.redRocket.Size = new System.Drawing.Size(200, 23);
            this.redRocket.TabIndex = 3;
            // 
            // redScore
            // 
            this.redScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.redScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.redScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.redScore.ForeColor = System.Drawing.Color.Red;
            this.redScore.Location = new System.Drawing.Point(25, 418);
            this.redScore.Name = "redScore";
            this.redScore.Size = new System.Drawing.Size(100, 23);
            this.redScore.TabIndex = 4;
            this.redScore.Text = "Red";
            // 
            // blueScore
            // 
            this.blueScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.blueScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.blueScore.ForeColor = System.Drawing.Color.Blue;
            this.blueScore.Location = new System.Drawing.Point(25, 12);
            this.blueScore.Name = "blueScore";
            this.blueScore.Size = new System.Drawing.Size(100, 23);
            this.blueScore.TabIndex = 5;
            this.blueScore.Text = "Blue";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(713, 418);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Exit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(496, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(310, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "محمد عرفان حمیدپور، ریحانه رضوانی";
            // 
            // buttonResetScore
            // 
            this.buttonResetScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonResetScore.Location = new System.Drawing.Point(527, 418);
            this.buttonResetScore.Name = "buttonResetScore";
            this.buttonResetScore.Size = new System.Drawing.Size(75, 23);
            this.buttonResetScore.TabIndex = 8;
            this.buttonResetScore.Text = "Reset Score";
            this.buttonResetScore.UseVisualStyleBackColor = true;
            this.buttonResetScore.Click += new System.EventHandler(this.buttonResetScore_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonResetScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.blueScore);
            this.Controls.Add(this.redScore);
            this.Controls.Add(this.redRocket);
            this.Controls.Add(this.blueRocket);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label blueRocket;
        private System.Windows.Forms.Label redRocket;
        private System.Windows.Forms.Label redScore;
        private System.Windows.Forms.Label blueScore;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonResetScore;
    }
}


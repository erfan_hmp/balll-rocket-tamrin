﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace rocket
{
    public partial class Form1 : Form
    {
        int xball;
        int yball;

        int xrocket;
        int xrockett;

        int red = 0;
        int blue = 0;

        int xdirect = 1;
        int ydirect = 1;

        int xDiff = 10;
        int yDiff = 10;

        int rocketsSpeed = 30;
        int ballSpeed = 15;

        public Form1()
        {
            InitializeComponent();

            redScore.Text = "Red: " + red;
            blueScore.Text = "Blue: " + blue;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Interval = 100;

            timer1.Start();
            button1.Visible = false;
            xball = this.Width / 2;
            yball = this.Height / 2;

            buttonResetScore.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            button1.Visible = true;
            buttonResetScore.Enabled = true;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
                xrocket += rocketsSpeed;

            if (e.KeyCode == Keys.Left)
                xrocket -= rocketsSpeed;

            blueRocket.Location = new Point(xrocket, blueRocket.Location.Y);

            if (e.KeyCode == Keys.D)
                xrockett += rocketsSpeed;

            if (e.KeyCode == Keys.A)
                xrockett -= rocketsSpeed;

            redRocket.Location = new Point(xrockett, redRocket.Location.Y);
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            xball += xDiff * xdirect;
            yball += yDiff * ydirect;

            if (xball < 0)
                xdirect = 1;

            if (xball + 70 > this.Width)
                xdirect = -1;

            if (yball < 0)
                ydirect = 1;


            if (yball > redRocket.Location.Y - 50)
            {
                if (xball > blueRocket.Location.X && xball < redRocket.Location.X + redRocket.Width)
                {
                    ydirect = -1;

                    //xDiff = new Random().Next(10, 20);
                    //yDiff = new Random().Next(10, 20);

                    if (timer1.Interval - ballSpeed > 0)
                        timer1.Interval -= ballSpeed;
                    else
                        timer1.Interval = 1;

                    red++;
                    redScore.Text = "Red: " + red;
                }
                else
                {
                    timer1.Stop();

                    for (int i = 0; i < 40; i++)
                    {
                        xball += i;
                        yball += i;

                        Refresh();
                    }

                    xball += 20;
                    yball += 20;

                    Refresh();

                    MessageBox.Show("game over red",
                    "Rocket Game",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                }
            }


            if (yball < blueRocket.Location.Y + 23)
            {
                if (xball > redRocket.Location.X && xball < blueRocket.Location.X + blueRocket.Width)
                {
                    ydirect = 1;

                    //xDiff = new Random().Next(10, 20);
                    //yDiff = new Random().Next(10, 20);

                    if (timer1.Interval - ballSpeed > 0)
                        timer1.Interval -= ballSpeed;
                    else
                        timer1.Interval = 1;

                    blue++;
                    blueScore.Text = "Blue: " + blue;
                }
                else
                {
                    timer1.Stop();

                    for (int i = 0; i < 40; i++)
                    {
                        xball -= i;
                        yball -= i;

                        Refresh();
                    }

                    xball -= 20;
                    yball -= 20;

                    MessageBox.Show("game over blue",
                        "Rocket Game",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
            this.Invalidate();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            xball = this.Width / 2;
             yball = this.Height / 2;

            xrocket = xball - redRocket.Width / 2;
            redRocket.Location = new Point(xrocket, redRocket.Location.Y);

            xrockett = xball - blueRocket.Width / 2;
            blueRocket.Location = new Point(xrockett, blueRocket.Location.Y);

            MessageBox.Show("For move blue rocket use control and keys left or right",
                "Rocket Game",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillEllipse(Brushes.Yellow, xball, yball, 50, 50);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonResetScore_Click(object sender, EventArgs e)
        {
            red = 0;
            blue = 0;

            redScore.Text = "Red: " + red;
            blueScore.Text = "Blue: " + blue;
        }
    }
}